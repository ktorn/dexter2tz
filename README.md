- [Dexter 2](./dexter.mligo) [(Compiled)](./dexter.mligo.tz)
- [Coq specification for Dexter 2](./dexter_spec.v)
- [FA1.2 implementation + custom mintOrBurn for LQT token](./lqt_fa12.mligo) [(Compiled)](./lqt_fa12.mligo.tz)

Contracts were compiled using ligo built from source at revision
4d10d07ca05abe0f8a5fb97d15267bf5d339d9f4. This version does automatic
uncurrying, but does _not_ yet use Edo-only instructions, which are
not yet supported in Mi-cho-coq.
